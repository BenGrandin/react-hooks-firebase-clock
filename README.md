# A realtime database Clock with React & Firebase
Starter made following a youtube tutorial from [Baylor Breaks It Down](https://www.youtube.com/watch?v=rSgbYCdc4G0)  

The starter contains only the list load from the firebase, and a create from.

I implement : 
- [MaterialUI](https://material-ui.com/) 
- Delete Icons

## Branches 

### Master
App.js is converted to class component, and have all the methods

### allHooks
 All components are hooks, their methods are inside them

## License

The repo is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).